# How does it work?
 
The initial step for creating first `Listings` is running the `EmpireFlippers::ListingImport` job, job is going to call a service that will connect to EF api and start importing new and valid Listings into the database. On the next run, the service will only fetch the data that ware updated after the latest run. After Listing is created, `after_create` hook will trigger the `Hubspot::DealCreation` job to create a deal in CRM. If listing was updated it will trigger `Hubspot::DealUpdate` job to sync the Listing with the Deal.

There were other implementations that I could have gone with, but I decided on this one thinking that `Listing` can be created from other places ( eg. api, UI ) and we would still want to create a deal in Hubspot.
 

 
# Installation guide
 
Since the app is fairly small, do the following steps:
1. Bundle install
2. Rails db:setup
3. Run Sidekiq
