require 'spec_helper'

describe EmpireFlippers::ListingsImporter, :vcr do
  let(:service) { described_class.new }
  let(:valid_attributes) do
    {
      summary: "Test summary!",
      listing_number: 40269,
      listing_status: "Sold",
      listing_price: 500,
      id: "551d077d-f9b9-4192-bed5-0138fefa6ccc"
    }
  end

  describe '#run', :vcr do
    context 'imports the listings to the database' do
      it 'creates new listings' do
        expect { service.run }.to change(Listing, :count)
      end

      it 'does not create new listing if it is created' do
        expect { service.run }.to change(Listing, :count)
        expect { service.run }.to_not change(Listing, :count)
      end
    end
  end

  describe '#fetch_listings', :vcr do
    it 'makes a request to EF api and receives relevant data' do
      response = service.send(:fetch_listings).to_h
      expect(response).to have_key(:listings)
      expect(response).to have_key(:pages)
    end
  end

  describe '#store_listings!' do
    context 'with valid attributes' do
      it 'creates a new Listing' do
        expect do
          service.send(:store_listings!, [ 
            OpenStruct.new(valid_attributes)
          ])
        end.to change(Listing, :count).by(1)
      end

      it 'updates a Listing' do
        listing = create :listing, uuid: valid_attributes[:id]

        service.send(:store_listings!, [
          OpenStruct.new(valid_attributes)
        ])

        expect(listing.reload.summary).to eq valid_attributes[:summary]
      end
    end

    context 'with invalid attributes' do
      it 'skips the invalid record' do
        expect do
          service.send(:store_listings!, [ 
            OpenStruct.new(valid_attributes.merge(id: 'invalid'))
          ])
        end.to change(Listing, :count).by(0)
      end
    end
  end
end
