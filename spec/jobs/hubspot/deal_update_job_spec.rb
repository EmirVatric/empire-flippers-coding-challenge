require 'spec_helper'

describe Hubspot::DealUpdateJob, type: :job do
  let(:job) { Hubspot::DealUpdateJob.new }
  let(:updater) { Hubspot::DealUpdater }
  let(:listing) { create :listing }

  describe '#perform' do
    it 'runs deal updater', :vcr do
      allow_any_instance_of(updater).to receive(:run)
      expect_any_instance_of(updater).to receive(:run)

      job.perform(listing)
    end
  end
end
