require 'spec_helper'

describe Hubspot::DealCreationJob, type: :job do
  let(:job) { Hubspot::DealCreationJob.new }
  let(:creator) { Hubspot::DealCreator }
  let(:listing) { create :listing }

  describe '#perform' do
    it 'runs deal creator', :vcr do
      allow_any_instance_of(creator).to receive(:run)
      expect_any_instance_of(creator).to receive(:run)

      job.perform(listing)
    end
  end
end
