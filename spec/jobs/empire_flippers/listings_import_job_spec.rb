require 'spec_helper'

describe EmpireFlippers::ListingsImportJob, type: :job do
  let(:job) { EmpireFlippers::ListingsImportJob.new }
  let(:importer) { EmpireFlippers::ListingsImporter }

  describe '#perform' do
    it 'runs Listing Imports', :vcr do
      allow_any_instance_of(importer).to receive(:run)
      expect_any_instance_of(importer).to receive(:run)

      job.perform
    end
  end
end
