require 'spec_helper'

describe Listing, type: :model  do
  let(:valid_attributes) do
    {
      uuid: "d4cd96ad-d76b-483d-b74c-be6e167f9ba4",
      number: 5555,
      price: 5000,
      status: "For Sale",
      summary: "Test!"
    }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:uuid) }
    it { is_expected.to validate_presence_of(:number) }
    it { is_expected.to validate_presence_of(:price) }
    it { is_expected.to validate_presence_of(:status) }
    it { is_expected.to validate_presence_of(:summary) }
  end

  describe '#create' do
    it 'creates a new Listing' do
      expect {
        create :listing, valid_attributes
      }.to change(Listing, :count).by(1)
    end

    context 'Hubspot' do
      it 'should queue hubspot deal creation job' do
        expect(Hubspot::DealCreationJob).to receive(:perform_later).once
        create :listing, valid_attributes
      end

      it 'should not queue hubspot deal creation job if status is not For Sale' do
        expect(Hubspot::DealCreationJob).to_not receive(:perform_later)
        create :listing
      end
    end
  end

  describe '#update' do
    let!(:listing) { create :listing, deal_id: 111, **valid_attributes }

    context 'Hubspot' do
      it 'should queue hubspot deal update job when relevant params are updated' do
        expect(Hubspot::DealUpdateJob).to receive(:perform_later).once
        listing.update(summary: "New Test Summary")

        expect(Hubspot::DealUpdateJob).to receive(:perform_later).once
        listing.update(number: 777)

        expect(Hubspot::DealUpdateJob).to receive(:perform_later).once
        listing.update(price: 222)
      end

      it 'should not queue hubspot deal update job when status is not for sale' do
        expect(Hubspot::DealUpdateJob).to_not receive(:perform_later)
        listing.update(status: "Sold")
      end

      it 'queues a new hubspot deal creation job when status is changed to For Sale and listing was not on sale before' do
        listing = create :listing
        expect(Hubspot::DealCreationJob).to receive(:perform_later).once

        listing.update(status: "For Sale")
      end
    end
  end
end
