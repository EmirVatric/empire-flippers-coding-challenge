FactoryBot.define do
  factory :listing do
    sequence :number, '000'
    sequence :price, '000'
    uuid { SecureRandom.uuid }
    summary { 'Test!' }
    status { 'Sold' }
  end
end
