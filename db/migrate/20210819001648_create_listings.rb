class CreateListings < ActiveRecord::Migration[6.1]
  def change
    create_table :listings do |t|
      t.integer :number
      t.uuid :uuid, index: true
      t.integer :price, :decimal, precision: 8, scale: 2
      t.string :status
      t.text :summary
      t.bigint :deal_id

      t.timestamps
    end
  end
end
