class EmpireFlippers::ListingsImportJob < ApplicationJob
  queue_as :default

  def self.schedule(date:)
    set(wait: 30.minutes).perform_later(date)
  end

  def perform(latest_run = Listing.last_EF_import)
    EmpireFlippers::ListingsImporter.new(latest_run).run
  end
end
