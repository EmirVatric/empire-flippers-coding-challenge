class Hubspot::DealUpdateJob < ApplicationJob
  queue_as :default

  def perform(listing)
    Hubspot::DealUpdater.new(listing).run
  end
end
