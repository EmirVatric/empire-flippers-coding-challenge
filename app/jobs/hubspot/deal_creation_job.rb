class Hubspot::DealCreationJob < ApplicationJob
  queue_as :default

  def perform(listing)
    Hubspot::DealCreator.new(listing).run
  end
end
