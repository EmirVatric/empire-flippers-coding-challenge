class Hubspot::DealCreator
  DEFAULTS = {
    closing_date: (30.days.from_now.to_f * 1000).round,
    deal_stage:   "qualifiedtobuy",
    pipeline:     "default",
    api:          [ nil, nil, nil ]
  }.freeze

  def initialize(listing)
    @listing = listing
  end

  def run
    return if Rails.env.test?
    deal = Hubspot::Deal.create!(*DEFAULTS[:api], deal_attributes)

    listing.update(deal_id: deal.properties["hs_object_id"])
  end

  private
  attr_reader :listing

  def deal_attributes
    {
      description: listing.summary,
      dealstage:   DEFAULTS[:deal_stage],
      closedate:   DEFAULTS[:closing_date],
      dealName:    "Listing #{listing.number}",
      pipeline:    DEFAULTS[:pipeline],
      amount:      listing.price
    }
  end
end
