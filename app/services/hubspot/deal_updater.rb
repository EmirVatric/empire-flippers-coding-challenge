class Hubspot::DealUpdater
  def initialize(listing)
    @listing = listing
  end

  def run
    return if Rails.env.test?
    Hubspot::Deal.find(listing.deal_id).update!(deal_attributes)
  end

  private
  attr_reader :listing

  def deal_attributes
    {
      description: listing.summary,
      dealName:    "Listing #{listing.number}",
      amount:      listing.price
    }
  end
end
