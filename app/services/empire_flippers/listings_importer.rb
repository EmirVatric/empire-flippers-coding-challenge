class EmpireFlippers::ListingsImporter
  attr_accessor :page

  DEFAULTS = {
    base_url: "https://api.empireflippers.com/api/v1/listings/list?",
    filters:  { limit: 100 }
  }.freeze

  def initialize(latest_run = Listing.last_EF_import)
    @latest_run = latest_run
    @page       = 1
  end

  def run
    import_listings
  end

  private
  attr_reader :latest_run

  def full_url
    URI(DEFAULTS[:base_url] + DEFAULTS[:filters].merge(page: page, updated_at_from: latest_run).to_param)
  end

  def fetch_listings
    response = Net::HTTP.get_response(full_url)
    reschedule unless response.kind_of? Net::HTTPSuccess

    JSON.parse(response.body, object_class: OpenStruct).data
  end

  def import_listings(response: fetch_listings)
    store_listings! response.listings
    return unless response.pages > page

    paginate_next; sleep 1; import_listings
  end

  def store_listings!(listings)
    return unless listings

    listings.each do |listing|
      if record = Listing.find_by(uuid: listing.id)
        record.update(listing_attributes(listing))
      else
        Listing.create(listing_attributes(listing))
      end
    end
  end

  def listing_attributes(listing)
    {
      summary: listing.summary,
      number:  listing.listing_number,
      status:  listing.listing_status,
      price:   listing.listing_price,
      uuid:    listing.id
    }
  end

  def reschedule
    EmpireFlippers::ListingsImportJob.schedule(date: latest_run)
  end

  def paginate_next
    self.page += 1
  end
end
