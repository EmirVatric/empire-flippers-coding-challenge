module Listing::Hubspot::Callbacks
  extend ActiveSupport::Concern

  included do
    after_create_commit :create_hubspot_deal_later, if: -> { status_for_sale? }
    after_update_commit :sync_with_hubspot, if: -> { status_for_sale? && hubspot_attribute_changed? }
  end

  private

  def status_for_sale?
    status == Listing::FOR_SALE_STATUS
  end

  def sync_with_hubspot
    deal_id.present? ? update_hubspot_deal_later : create_hubspot_deal_later
  end

  def hubspot_attribute_changed?
    saved_change_to_status? ||
    saved_change_to_summary? ||
    saved_change_to_number? ||
    saved_change_to_price?
  end

  def create_hubspot_deal_later
    Hubspot::DealCreationJob.perform_later(self)
  end

  def update_hubspot_deal_later
    Hubspot::DealUpdateJob.perform_later(self)
  end
end
