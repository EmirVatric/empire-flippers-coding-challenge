class Listing < ApplicationRecord
  include Hubspot::Callbacks

  FOR_SALE_STATUS = "For Sale".freeze

  validates :uuid, uniqueness: true
  validates_presence_of :uuid, :number, :price, :status, :summary

  def self.last_EF_import
    where.not(deal_id: nil).last&.created_at
  end
end
